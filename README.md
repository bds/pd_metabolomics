# Table of contents
* [Introduction](#introduction)
* [Content](#content)
* [Data](#data)
* [Requirements](#requirements)
* [License](#license)
* [Instructions](#instructions)

# Introduction
This repository contains the code for statistical analyses described in the article "Comprehensive blood metabolomics profiling of Parkinson’s disease reveals coordinated alterations in xanthine metabolism".

This project conducted blood plasma metabolic profiling of *de novo* and treated Parkinson's disease (PD) patients and controls, identifying significant alterations in metabolite levels and pathway activities, particularly in xanthine metabolism.

# Content
The code covers the following main analysis steps:

1. Loading, preparation and processing of datasets
2. Metabolomics differential abundance analysis for *de novo* PD vs. control and all PD vs. control
3. Visualizations (volcano plots)
4. Pathway enrichment analysis using MetaboAnalyst
5. Transcriptomics differential analysis for PD vs. control to compare against the metabolomics data


# Data
The public transcriptomics data used in this project was derived from the Gene Expression Omnibus (https://www.ncbi.nlm.nih.gov/geo/, ID: GSE8397).

# Requirements
The code was written entirely in R. It relies on multiple R and BioConductor packages, listed at the beginning of the corresponding R scripts.

# License
The code is available under the MIT License.

# Instructions
The code was tested to run in R4.2.0 on both current Mac (Ventura) and Linux operating systems (Ubuntu 23.04), but should be compatible with later versions of R installed on current Mac, Linux or Windows systems.
R software packages loaded at the beginning of the R scripts must be installed before using the code. R packages available on CRAN can be installed with the command:

install.packages("BiocManager::install("ADD_NAME_OF_THE_PACKAGE")

R packages from Bioconductor can be installed with the following commands:

if (!require("BiocManager", quietly = TRUE))
    install.packages("BiocManager")

BiocManager::install("ADD_NAME_OF_THE_PACKAGE")

To run the code, the correct working directory containing the input data must be specified at the beginning of the R-scripts, otherwise the scripts can be run as-is.