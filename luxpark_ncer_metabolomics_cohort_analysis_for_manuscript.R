# Load required libraries
require('xlsx')
require('ggplot2')
require('ggrepel')
require('limma')
require('multtest')
library('caret')
library('e1071')
library('ggplot2')


# Set working directory (choose the folder containing the sub-folders "Data" and "Analysis" with the data files and analysis scripts)
# setwd('/SET_TO_WORKING_DIRECTORY/')

# Load datasets
batchnormdat <- read.table('Data/LUXE-01-20PHML+ DATA TABLES_batch-norm_imputed_data.txt', header = TRUE, sep = "\t", comment.char = "", quote = "")
logtransdat <- read.table('Data/LUXE-01-20PHML+ DATA TABLES_log_transformed_data.txt', header = TRUE, sep = "\t", comment.char = "", quote = "")
metadat <- read.table('Data/metabolon 20210603 Sample metadata with ND tab.txt', header = TRUE, sep = "\t", comment.char = "", quote = "")
load(file = "Data/lux_park.clinical.Rdata")
chemicaldat <- read.table('Data/LUXE-01-20PHML_annotations_updatedJan2023.txt', header = TRUE, sep = "\t", comment.char = "", quote = "")
denovos <- read.xlsx('Data/De novo_LuxPark_PD_4_2022.xlsm', 1)


# Prepare and process data
batchnormnum <- apply(batchnormdat[, 2:ncol(batchnormdat)], 2, as.numeric)
rownames(batchnormnum) <- as.matrix(batchnormdat[, 1])

logtransnum <- apply(logtransdat[, 2:ncol(logtransdat)], 2, as.numeric)
rownames(logtransnum) <- as.matrix(logtransdat[, 1])

mapid <- match(rownames(logtransnum), metadat$PARENT_SAMPLE_NAME)
visits <- metadat[mapid, 2]

vis1 <- which(visits == "Visit 1")
vis2 <- which(visits == "Visit 2")
vis3 <- which(visits == "Visit 3")
vis4 <- which(visits == "Visit 4")
vis5 <- which(visits == "Visit 5")

mapchem <- match(as.numeric(substr(colnames(logtransnum), 2, 100)), chemicaldat$CHEM_ID)
colnames(logtransnum) <- chemicaldat$CHEMICAL_NAME[mapchem]

datvis1 <- logtransnum[vis1,]
rownames(datvis1) <- as.matrix(metadat[mapid[vis1], 1])

mapdenovo <- match(denovos$Subject.ID, rownames(datvis1))
mapdenovo_notna <- mapdenovo[which(!is.na(mapdenovo))]
datvis1_denovo <- datvis1[mapdenovo_notna,]
datvis1_denovo <- as.data.frame(datvis1_denovo)

mapids <- match(rownames(datvis1), datmat$cdisc_dm_usubjd)
visit0ind <- which(datmat$redcap_event_name == 0)

# Process additional variables
gender <- datmat$cdisc_dm_sex[mapids]
mapids0 = match(rownames(datvis1), datmat$cdisc_dm_usubjd[visit0ind])
age0 = datmat$sv_age[visit0ind][mapids0]

# Extract final diagnosis data
fdiag = datmat$adiag_final_diagnosis[mapids]

# Identify indices for PD and control subjects
pdids = which(fdiag == 1)
contids = which(fdiag == 14)

# Create outcome variables for PD vs. control and de novo PD vs. control
denovoids = mapdenovo_notna
outcome = c(rep("iPD", length(pdids)), rep("Ctrl", length(contids)))
outcome_denovo = c(rep("dPD", length(denovoids)), rep("Ctrl", length(contids)))

# Perform linear modeling and analysis for baseline
std0denovo = which(apply(datvis1[c(denovoids, contids), ], 2, var) == 0)
design <- model.matrix(~factor(gender[c(denovoids, contids)]) + scale(age0[c(denovoids, contids)]) + factor(outcome_denovo))
colnames(design)[2:ncol(design)] <- c("gender", "age", "outcome")
fit <- lmFit(t(datvis1[c(denovoids, contids), -std0denovo]), design)
fit2 <- eBayes(fit)
ttable_denovo_baseline <- topTable(fit2, coef = "outcome", n = Inf)

# Write baseline results to Excel
setwd('Results/')
write.xlsx(ttable_denovo_baseline, file = "luxpark_metabolomics_pddenovo_vs_cont_2021_adjusted_age_gender_baseline.xlsx")


# Define and execute function for Volcano plot
plotvolcano <- function(dat, logfc, fdr, logfc_thresh1 = 1, fdr_thresh = 0.05, lablogfc_thresh = 1.5, labfdr_thresh = 0.001, cexlab = 2, filestr = "volcano_plot_with_labels.pdf") {
  mat = data.frame(x = logfc, y = -log10(fdr))
  matorange = data.frame(x = logfc, y = -log10(fdr))[which(abs(logfc) > logfc_thresh1), ]
  matgreen = data.frame(x = logfc, y = -log10(fdr))[intersect(which(fdr < fdr_thresh), which(abs(logfc) > logfc_thresh1)), ]
  matred = data.frame(x = logfc, y = -log10(fdr))[which(fdr < fdr_thresh), ]

  matlab = data.frame(x = logfc, y = -log10(fdr))[intersect(which(fdr < labfdr_thresh), which(abs(logfc) > lablogfc_thresh)), ]

  topgenes = rownames(dat)
  matlabels = topgenes[intersect(which(fdr < labfdr_thresh), which(abs(logfc) > lablogfc_thresh))]

  if (!require('ggrepel')) {
    install.packages("ggrepel")
  }
  require('ggplot2')
  # Write volcano plot to pdf
  pdf(filestr)
  p = ggplot(mat, aes(x, y)) +
    geom_point(color = 'gray', size = 0.8, fill = "white") +
    theme_classic(base_size = 16) +
    geom_point(data = matorange, color = "orange") +
    geom_point(data = matred, color = "red") +
    geom_point(data = matgreen, color = "green") +
    geom_text_repel(data = matlab, label = matlabels, nudge_y = 0.1, max.time=100, max.iter=1000000, size = cexlab, seed=123) +
    xlab('Log fold change') +
    ylab('-log10(P-value)') +
    theme(axis.text = element_text(size = 12), axis.title = element_text(size = 12, face = "bold"))
  print(p)
  dev.off()
}

plotvolcano(ttable_denovo_baseline, ttable_denovo_baseline$logFC, ttable_denovo_baseline$adj, logfc_thresh1 = 0.3, fdr_thresh = 0.05, lablogfc_thresh = 0.1, labfdr_thresh = 0.05, filestr = "luxpark_metabolomics_pddenovo_vs_cont_2023_age_gender_adjust_baseline_volcano_plot_with_labels_manuscript.pdf", cexlab = 3)


# Perform additional analysis for all PD patients and write results to Excel
std0allpd = which(apply(datvis1[c(union(pdids,denovoids), contids),], 2, var)==0)
ldopa <- datvis1[, match("3-methoxytyrosine", colnames(datvis1))]
outcomeall = c(rep("iPD", length(union(pdids,denovoids))), rep("Ctrl", length(contids)))
design <- model.matrix(~factor(gender[c(union(pdids,denovoids), contids)]) + age0[c(union(pdids,denovoids), contids)] + ldopa[c(union(pdids,denovoids), contids)] + factor(outcomeall))
colnames(design)[2:ncol(design)] <- c("gender", "age", "ldopa", "outcomeall")
fit <- lmFit(t(datvis1[c(union(pdids,denovoids), contids), -std0allpd]), design)
fit2 <- eBayes(fit)
ttable_baseline <- topTable(fit2, coef = "outcomeall", n = Inf)


# Apply correlation filter
std0 = which(apply(datvis1, 2, var)==0)
all_ldopa_cor = apply(datvis1[,-std0], 2, function(x) cor(x, ldopa, method="spear"))
corfeat2 = which(abs(all_ldopa_cor) > 0.2)
ttable_corfeat2filtall = ttable_baseline[-match(names(corfeat2),rownames(ttable_baseline)),]

# Write correlation-filtered results to Excel
write.xlsx(ttable_corfeat2filtall, file = "luxpark_metabolomics_pdall_vs_cont_adjustment_2023_corfeat2filt_for_manuscript_supplmat.xlsx")

# Compare de novo PD vs. control against all-PD vs. control results and determine the overlap of significant findings
sharedsig2corfilt = intersect(
  rownames(ttable_denovo_baseline)[which(ttable_denovo_baseline$adj < 0.05)],
  rownames(ttable_corfeat2filt)[which(ttable_corfeat2filt$adj < 0.05)]
)
ttable_overlap = cbind(
  ttable_denovo_baseline[match(sharedsig2corfilt, rownames(ttable_denovo_baseline)),],
  ttable_corfeat2filt[match(sharedsig2corfilt, rownames(ttable_corfeat2filt)),]
)
write.xlsx(ttable_overlap, file = "luxpark_metabolomics_pddenovo_vs_cont_allpd_vs_cont_2021_overlap.xlsx")


# Perform additional analysis for treated PD and write results to Excel
treatedids = setdiff(pdids, denovoids)
outcome_treated = c(rep("iPD", length(treatedids)), rep("Ctrl", length(contids)))
std0treatedpd = which(apply(datvis1[c(treatedids, contids),], 2, var)==0)
ldopa <- datvis1[, match("3-methoxytyrosine", colnames(datvis1))]
design <- model.matrix(~factor(gender[c(treatedids, contids)]) + age0[c(treatedids, contids)] + ldopa[c(treatedids, contids)] + factor(outcome_treated))
colnames(design)[2:ncol(design)] <- c("gender", "age", "ldopa", "outcome_treated")
fit <- lmFit(t(datvis1[c(treatedids, contids), -std0treatedpd]), design)
fit2 <- eBayes(fit)
ttable_treated_baseline <- topTable(fit2, coef = "outcome_treated", n = Inf)

# Apply correlation filter
std0 = which(apply(datvis1, 2, var)==0)
all_ldopa_cor = apply(datvis1[,-std0], 2, function(x) cor(x, ldopa, method="spear"))
corfeat2 = which(abs(all_ldopa_cor) > 0.2)
ttable_treated_corfeat2filt = ttable_treated_baseline[-match(names(corfeat2),rownames(ttable_treated_baseline)),]

# Write correlation-filtered results to Excel
write.xlsx(ttable_treated_corfeat2filt, file = "luxpark_metabolomics_treatedpd_vs_cont_adjustment_2023_corfeat2filt_for_manuscript_supplmat.xlsx")


# Perform linear model analysis for UPDRS3 associations
mapids0 = match(rownames(datvis1), datmat$cdisc_dm_usubjd[visit0ind])
age0 = datmat$sv_age[visit0ind][mapids0]
updrs3sum0 = datmat$u_part3_score[visit0ind][mapids0]
lmres = matrix(0.0, ncol = 2, nrow = ncol(datvis1))
narmpd0 = which(is.na(updrs3sum0[pdids]))

# Loop through metabolites to perform linear model analysis
for (i in 1:ncol(datvis1[pdids[-narmpd0], ])) {
  lmmod = lm(datvis1[pdids[-narmpd0], i] ~ factor(gender[pdids][-narmpd0]) +
              age0[pdids][-narmpd0] + ldopa[pdids][-narmpd0] + updrs3sum0[pdids][-narmpd0])
  lmres[i,] = c(summary(lmmod)$coef[5, 1], summary(lmmod)$coef[5, 4])
}

rownames(lmres) = colnames(datvis1)
colnames(lmres) = c("Effect estimate", "P-value") 

# Prepare data for MetaboAnalyst
lmres_kegg = lmres
rownames(lmres_kegg) = chemicaldat$KEGG[mapchem]

# Perform multiple testing adjustment
padj = mt.rawp2adjp(lmres[, 2], proc = "BH", alpha = 0.05, na.rm = FALSE)

# Combine results and display
head(cbind(lmres[padj$index, ], padj$adjp[, c(1, 2)]), 10)

# Add Spearman correlation
all_updrs3sum_cor0 = apply(datvis1[pdids[-narmpd0], ], 2, function(x) cor(x, updrs3sum0[pdids][-narmpd0], method = "spear"))
lmres_comb = cbind(all_updrs3sum_cor0[padj$index], lmres[padj$index, ], padj$adjp[, c(1, 2)])
head(lmres_comb, 20)

# Format and write results to Excel
lmres_comb = as.data.frame(lmres_comb)
sig_updrs3 = which(lmres_comb$BH < 0.05)
colnames(lmres_comb)[1] = "Spearman's rho"
write.xlsx(lmres_comb[sig_updrs3, ], file = "luxpark_metabolomics_pdvis1_vs_updrs3sum_v0_sig_2023_adjusted_age_gender_ldopa.xlsx")

# Process differentially abundant metabolites for MetaboAnalyst
denovo_pval_degs = rownames(ttable_denovo_baseline)[which(ttable_denovo_baseline$P.Val < 0.05)]
hmdb_ids = as.character(sapply(chemicaldat$HMDB[match(denovo_pval_degs, chemicaldat$CHEMICAL_NAME)], function(x) strsplit(x, ",")[[1]][1]))
hmdb_ids[which(is.na(hmdb_ids))] = rep("", length(which(is.na(hmdb_ids))))
denovo_pval_degs[which(hmdb_ids != "")] = hmdb_ids[which(hmdb_ids != "")]
denovo_pval_degs = gsub("\\*", "", denovo_pval_degs)
denovo_pval_degs = denovo_pval_degs[grep("HMDB",denovo_pval_degs)]

# Write HMDB identifiers for MetaboAnalyst
write.xlsx(denovo_pval_degs, file = "luxpark_metabolomics_pddenovo_vs_cont_adjusted_age_gender_HMDB_identifiers_for_metaboanalyst.xlsx", row.names = FALSE, col.names = FALSE)

# Write background set / reference metabolome for MetaboAnalyst
hmdb_ids = as.character(sapply(chemicaldat$HMDB[match(rownames(ttable_denovo_baseline), chemicaldat$CHEMICAL_NAME)], function(x) strsplit(x,",")[[1]][1]))
hmdb_ids[which(is.na(hmdb_ids))] = rep("",length(which(is.na(hmdb_ids))))
hmdb_ids = setdiff(hmdb_ids,"")
write.table(hmdb_ids, file="luxpark_metabolomics_reference_metabolome.txt", sep = '\t', row.names = FALSE, col.names= FALSE, quote = FALSE) 



# Machine learning analyses

# Denovo vs. control data
df <- data.frame(datvis1[c(denovoids, contids), -std0denovo])
df$response <- factor(c(rep("denovo",length(denovoids)), rep("control",length(contids))), labels = c("Negative", "Positive"))


# Initialize vectors to store AUC values
train_linear_auc <- numeric(ncol(df) - 1)
train_radial_auc <- numeric(ncol(df) - 1)
test_linear_auc <- numeric(ncol(df) - 1)
test_radial_auc <- numeric(ncol(df) - 1)

# Split the data into training (66%) and test (34%) sets
trainIndex <- createDataPartition(df$response, p = .66, list = FALSE)
trainData <- df[trainIndex,]
testData  <- df[-trainIndex,]

folds <- 5

# Loop through each feature
for (i in 1:(ncol(df) - 1)) {

	cat(paste("Current iteration:",i,"\n"))


  feature_train <- trainData[, i, drop=FALSE]
  feature_test <- testData[, i, drop=FALSE]

	cvIndex <- createFolds(factor(trainData$response), folds, returnTrain = T)

  
  # Linear SVM
  set.seed(123)
  linear_model <- train(feature_train, trainData$response,
                        method = "svmLinear",
                        # , sampling = "up"
                        trControl = trainControl(index = cvIndex, method = "cv", number=folds, classProbs = TRUE, summaryFunction = twoClassSummary),
                        tuneGrid = expand.grid(C = seq(0.1, 1, by = 0.1)),
                        metric = "ROC")
  
  best_auc_linear <- max(linear_model$results$ROC)
  sd_linear <- sd(linear_model$results$ROC)
  least_complex_linear <- linear_model$results %>%
    filter(ROC >= (best_auc_linear - sd_linear)) %>%
    arrange(C) %>%
    head(1)
 
# Re-train linear SVM with the chosen hyperparameters
final_linear_model <- train(feature_train, trainData$response,
                            method = "svmLinear",
                            trControl = trainControl(method = "none", classProbs = TRUE, summaryFunction = twoClassSummary),
                            tuneGrid = data.frame(C = least_complex_linear$C),
                            metric = "ROC")
  
  # Radial SVM
  set.seed(123)
  radial_model <- train(feature_train, trainData$response,
                        method = "svmRadial",
                        trControl = trainControl(index = cvIndex, method = "cv", number=folds, classProbs = TRUE, summaryFunction = twoClassSummary),
                        tuneGrid = expand.grid(sigma = seq(0.05, 0.2, by = 0.05), C = seq(1, 5, by = 1)),
                        metric = "ROC")
  
  best_auc_radial <- max(radial_model$results$ROC)
  sd_radial <- sd(radial_model$results$ROC)
  least_complex_radial <- radial_model$results %>%
    filter(ROC >= (best_auc_radial - sd_radial)) %>%
    arrange(desc(sigma), C) %>%
    head(1)
  
# Re-train radial SVM with the chosen hyperparameters
final_radial_model <- train(feature_train, trainData$response,
                            method = "svmRadial",
                            trControl = trainControl(method = "none", classProbs = TRUE, summaryFunction = twoClassSummary),
                            tuneGrid = data.frame(sigma = least_complex_radial$sigma, C = least_complex_radial$C),
                            metric = "ROC")
  
  # Store cross-validated AUCs
  train_linear_auc[i] <- max(least_complex_linear$ROC)
  train_radial_auc[i] <- max(least_complex_radial$ROC)
  
  # Test the final models on the test set
  linear_predictions <- predict(final_linear_model, feature_test, type = "prob")
  radial_predictions <- predict(final_radial_model, feature_test, type = "prob")
  
  # Get AUC for the test set and store them
  test_linear_auc[i] <- roc(testData$response, linear_predictions$Positive)$auc
  test_radial_auc[i] <- roc(testData$response, radial_predictions$Positive)$auc
}

# Save AUC values to a data frame
results <- data.frame(
  Feature = colnames(datvis1[c(denovoids, contids), -std0denovo]),
  Train_Linear_AUC = train_linear_auc,
  Test_Linear_AUC = test_linear_auc,
  Train_Radial_AUC = train_radial_auc,
  Test_Radial_AUC = test_radial_auc
)


# Calculate the average AUC for each feature
results$Average_AUC <- rowMeans(results[, c("Train_Linear_AUC", "Test_Linear_AUC", "Train_Radial_AUC", "Test_Radial_AUC")])

# Sort the rows by the best average AUC in descending order
sorted_results <- results %>% 
  arrange(desc(Average_AUC))

# Write results to Excel
write.xlsx(sorted_results, file = "luxpark_metabolomics_denovo_pd_vs_control_machine_learning_classification_results.xlsx", row.names=F)


# Use treated PD data only for UPDRS motor score prediction
std0treatedpdonly = which(apply(datvis1[treatedids,], 2, var)==0)
treatedpd_data <- data.frame(datvis1[treatedids, -std0treatedpdonly])
treatedpd_data$outcome <- updrs3sum0[treatedids]

treatedpd_data$outcome[which(is.na(treatedpd_data$outcome))] <- rep(median(treatedpd_data$outcome, na.rm=T), length(which(is.na(treatedpd_data$outcome))))

mapids0 = match(rownames(datvis1), datmat$cdisc_dm_usubjd[visit0ind])
updrs3sum0 = datmat$u_part3_score[visit0ind][mapids0]


# Split the dataset into training and test sets
set.seed(123)
training_indices <- createDataPartition(treatedpd_data$outcome, p = 0.66, list = FALSE)
training_set <- treatedpd_data[training_indices, ]
test_set <- treatedpd_data[-training_indices, ]

# Initialize a data frame to store the results
results_df <- data.frame(
  Feature = character(),
  Linear_MAE = numeric(),
  Linear_Rsq = numeric(),
  Radial_MAE = numeric(),
  Radial_Rsq = numeric(),
  stringsAsFactors = FALSE
)

# TrainControl setup for 5-fold cross-validation
ctrl <- trainControl(method = "cv", number = 5, search = "grid")

# Loop over all input features
for (feature_name in setdiff(names(treatedpd_data), "outcome")) {
  
  # Prepare data for training
  feature_train <- training_set[, c(feature_name, "outcome"), drop = FALSE]
  feature_test <- test_set[, c(feature_name, "outcome"), drop = FALSE]

  # Linear SVM Model
  set.seed(123)
  linear_model <- train(outcome ~ .,
                        data = feature_train,
                        method = "svmLinear",
                        trControl = ctrl,
                        preProcess = c("center", "scale"), tuneGrid = expand.grid(C = 10^seq(-3, 3, by = 1)),
                        metric = "RMSE")

  # Find the least complex model within one standard deviation of the best model for Linear SVM
  best_linear <- min(linear_model$results$RMSE)
  sd_linear <- sd(linear_model$results$RMSE)
  optimal_linear <- linear_model$results[linear_model$results$RMSE <= (best_linear + sd_linear), ] 
  optimal_linear <- optimal_linear[which.min(optimal_linear$C), ]

	if(nrow(optimal_linear)==0){
  	results_df <- rbind(results_df, data.frame(
    Feature = feature_name,
    Linear_RMSE = NA,
    Linear_Rsq = NA,
    Radial_RMSE = NA,
    Radial_Rsq = NA
  	))
  	next
  }
  

  # Radial SVM Model
  set.seed(123)
  radial_model <- train(outcome ~ .,
                        data = feature_train,
                        method = "svmRadial",
                        trControl = ctrl,
                        preProcess = c("center", "scale"), tuneGrid = expand.grid(sigma = 10^seq(-3, 0, by = 1), C = 10^seq(-3, 3, by = 1)),
                        metric = "RMSE")
  
  # Find the least complex model within one standard deviation of the best model for Radial SVM
  best_radial <- min(radial_model$results$RMSE)
  sd_radial <- sd(radial_model$results$RMSE)
  optimal_radial <- radial_model$results[radial_model$results$RMSE <= (best_radial + sd_radial), ]
  optimal_radial <- optimal_radial[which.min(optimal_radial$C), ]


  # Test the selected models on the test set
  linear_tune_grid <- expand.grid(C = optimal_linear$C)
    optimal_linear_model <- train(outcome ~ .,
                                  data = feature_train,
                                  method = "svmLinear",
                                  trControl = ctrl,
                                  preProcess = c("center", "scale"),
                                  tuneGrid = linear_tune_grid,
                                  metric = "RMSE")
    linear_pred <- predict(optimal_linear_model, newdata = feature_test)
    

	radial_tune_grid <- expand.grid(C = optimal_radial$C, sigma = optimal_radial$sigma)
    optimal_radial_model <- train(outcome ~ .,
                                  data = feature_train,
                                  method = "svmRadial",
                                  trControl = ctrl,
                                  preProcess = c("center", "scale"),
                                  tuneGrid = radial_tune_grid,
                                  metric = "RMSE")
    radial_pred <- predict(optimal_radial_model, newdata = feature_test)
    
  # Calculate MAE and R-squared for the test set  
  linear_rmse <- sqrt(mean((linear_pred - feature_test$outcome)^2))
  linear_rsq <- cor(linear_pred, feature_test$outcome)^2
  
  radial_rmse <- sqrt(mean((radial_pred - feature_test$outcome)^2))
  radial_rsq <- cor(radial_pred, feature_test$outcome)^2
  
  # Store the results
  results_df <- rbind(results_df, data.frame(
    Feature = feature_name,
    Linear_RMSE = linear_rmse,
    Linear_Rsq = linear_rsq,
    Radial_RMSE = radial_rmse,
    Radial_Rsq = radial_rsq
  ))
}


# Rank the features based on MAE and R-squared, lower rank is better for MAE, higher rank is better for R-squared
results_df <- results_df %>%
  mutate(Linear_RMSE_Rank = rank(Linear_RMSE),
         Linear_Rsq_Rank = rank(desc(Linear_Rsq)),
         Radial_RMSE_Rank = rank(Radial_RMSE),
         Radial_Rsq_Rank = rank(desc(Radial_Rsq)),
         Sum_of_Ranks = Linear_RMSE_Rank + Linear_Rsq_Rank + Radial_RMSE_Rank + Radial_Rsq_Rank)


# Sort the data frame by Sum_of_Ranks to identify the best-performing feature
sorted_results_updrs <- results_df %>%
  arrange(Sum_of_Ranks)

ord = order(results_df$Sum_of_Ranks)
sorted_feature_names = (colnames(datvis1)[-std0treatedpd])[ord]

sorted_results_updrs$Feature = sorted_feature_names

# Write baseline results to Excel
write.xlsx(sorted_results_updrs, file = "luxpark_metabolomics_updrs_treatedpd_machine_learning_regression_results.xlsx", row.names=F)
